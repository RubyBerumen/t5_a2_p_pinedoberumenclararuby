'''
Created on 29 nov. 2020

@author: Ruby
'''
import random
import copy
import time

class NumerosAleatorios:
    def generarNumeros(self,cantidad):
        nums = []
        for i in range(cantidad):
            nums.append(random.randint(0,cantidad))
        return nums

class MetodosOrdenamiento:
    
    class Burbuja:
        
        def ordenacionBurbuja1(self, nums):
            numeros=nums.copy()
            recorridos = 0
            comparaciones = 0
            intercambios = 0
            tiempo = 0
            
            tInicio = time.time_ns()
            recorridos+=1
            for i in range (1,len(numeros)):
                recorridos+=1
                for j in range (len(numeros)-i):
                    comparaciones+=1
                    if(numeros[j]>numeros[j+1]):
                        intercambios+=1
                        aux = numeros[j]
                        numeros[j]=numeros[j+1]
                        numeros[j+1]=aux
                        
            tFin = time.time_ns()
            tiempo = tFin-tInicio
            print(f"Burbuja1:     {tiempo}    {recorridos}    {comparaciones}    {intercambios}")

                    
        def ordenacionBurbuja2(self, nums):
            numeros=nums.copy()
            recorridos = 0
            comparaciones = 0
            intercambios = 0
            tiempo = 0
            
            tInicio = time.time_ns()
            i=1
            ordenado=False
            recorridos+=1
            while (i<len(numeros)):
                ordenado=True
                recorridos+=1
                for j in range(len(numeros)-i):
                    comparaciones+=1
                    if(numeros[j]>numeros[j+1]):
                        intercambios+=1
                        ordenado=False
                        aux = numeros[j]
                        numeros[j]=numeros[j+1]
                        numeros[j+1]=aux
                i+=1
            tFin = time.time_ns()
            tiempo = tFin-tInicio
            print(f"Burbuja2:     {tiempo}    {recorridos}    {comparaciones}    {intercambios}")
    
        def ordenacionBurbuja3(self, nums):
            numeros=nums.copy()
            recorridos = 0
            comparaciones = 0
            intercambios = 0
            tiempo = 0
            
            tInicio = time.time_ns()
            i=1
            ordenado=True
            recorridos+=1
            for j in range(len(numeros)-1): 
                comparaciones+=1            
                if(numeros[j]>numeros[j+1]):
                    intercambios+=1
                    ordenado=False
                    aux = numeros[j]
                    numeros[j]=numeros[j+1]
                    numeros[j+1]=aux
            i+=1
            while (i<len(numeros)):
                ordenado=True
                recorridos+=1
                for j in range(len(numeros)-i):
                    if(numeros[j]>numeros[j+1]):
                        intercambios+=1
                        ordenado=False
                        aux = numeros[j]
                        numeros[j]=numeros[j+1]
                        numeros[j+1]=aux
                i+=1 
            tFin = time.time_ns()
            tiempo = tFin-tInicio
            print(f"Burbuja3:     {tiempo}    {recorridos}    {comparaciones}    {intercambios}")
     


    class Insercion:
    
        def ordenarInsercion(self, nums):
            numeros=nums.copy()
            recorridos = 0
            comparaciones = 0
            intercambios = 0
            tiempo = 0
            
            tInicio = time.time_ns()
            
            aux=0
            recorridos+=1
            for i in range (1,len(numeros)):
                aux=numeros[i]
            
                j=(i-1)
                recorridos+=1
                comparaciones+=1
                while(j>=0 and numeros[j]>aux):
                    comparaciones+=1
                    intercambios+=1
                    numeros[j+1]=numeros[j]
                    numeros[j]=aux
                    j-=1
            tFin = time.time_ns()
            tiempo = tFin-tInicio
            print(f"Insercion:    {tiempo}    {recorridos}    {comparaciones}    {intercambios}")
     


    class Seleccion:
        
        def ordenamientoSeleccion(self, nums):
            numeros=nums.copy()
            recorridos = 0
            comparaciones = 0
            intercambios = 0
            tiempo = 0
            
            tInicio = time.time_ns()
            recorridos+=1
            for i in range(len(numeros)):
                recorridos+=1
                for j in range(i,len(numeros)):
                    comparaciones+=1
                    if (numeros[i] > numeros[j]):
                        intercambios+=1
                        minimo = numeros[i]
                        numeros[i]=numeros[j]
                        numeros[j]=minimo
            tFin = time.time_ns()
            tiempo = tFin-tInicio
            print(f"Seleccion:    {tiempo}    {recorridos}    {comparaciones}    {intercambios}")
     

    class Quicksort:
        def __init__(self):
            self.comparaciones = 0
            self.intercambios = 0
            self.recorridos = 0
            self.tiempo = 0
        
        def ordenar(self, numeros, izq, der):
            pivote = numeros[izq]
            i = izq
            j = der
            aux = 0
            self.recorridos+=1
            while i < j:
                self.recorridos+=1
                while numeros[i] <= pivote and i<j:
                    self.comparaciones+=1
                    i+=1
                self.recorridos+=1
                while numeros[j] > pivote:
                    j-=1
                self.comparaciones+=1;
                if i < j:
                    self.intercambios+=1
                    aux = numeros[i]
                    numeros[i] = numeros[j]
                    numeros[j] = aux
            self.intercambios+=1   
            numeros[izq] = numeros[j]
            numeros[j] = pivote
        
            if izq < j-1:
                self.ordenar(numeros, izq, j-1)
            if j+1 < der:
                self.ordenar(numeros, j+1, der)
        def llamadaQuicksort(self,nums):
            numeros=nums.copy()
            
            tInicio = time.time_ns()
            qs = MetodosOrdenamiento.Quicksort()
            qs.ordenar(numeros,0,(len(numeros) - 1))
            tFin = time.time_ns()
            tiempo = tFin-tInicio
            print(f"Quicksort:    {tiempo}     {qs.recorridos}    {qs.comparaciones}      {qs.intercambios}")    
                
    class Shellsort:
    
        def ordenar(self,nums):
            numeros=nums.copy()
            recorridos = 0
            comparaciones = 0
            intercambios = 0
            tiempo = 0
            
            tInicio = time.time_ns()
            intervalo = len(numeros)/2
            intervalo = int(intervalo)
            recorridos+=1
            while(intervalo>0):
                recorridos+=1
                for i in range(int(intervalo),len(numeros)):
                    j=i-int(intervalo)
                    recorridos+=1
                    while(j>=0):
                        k=j+int(intervalo)
                        comparaciones+=1
                        if(numeros[j] <= numeros[k]):
                            j-=1
                        else:
                            intercambios+=1
                            aux=numeros[j]
                            numeros[j]=numeros[k]
                            numeros[k]=aux
                            j-=int(intervalo)
            
                intervalo=int(intervalo)/2
            tFin = time.time_ns()
            tiempo = tFin-tInicio
            print(f"Shellsort:    {tiempo}    {recorridos}    {comparaciones}    {intercambios}")
     
     
     
    class Radix:            
        def __init__(self):
            self.comparaciones = 0
            self.intercambios = 0
            self.recorridos = 0
            self.tiempo = 0   

        def countingSort(self, arr, exp1):
            
            n = len(arr)
            output = [0] * (n)
            count = [0] * (10)
            
            self.recorridos+=1
            for i in range(0, n):
                index = (arr[i]/exp1)
                count[int((index)%10)] += 1
            self.recorridos+=1
            for i in range(1,10):
                self.intercambios+=1
                count[i] += count[i-1]
            i = n-1
            self.recorridos+=1
            while i>=0:
                self.intercambios+=1
                index = (arr[i]/exp1)
                output[ count[ int((index)%10) ] - 1] = arr[i]
                count[int((index)%10)] -= 1
                i -= 1

            i = 0
            for i in range(0,len(arr)):
                arr[i] = output[i]
                

        def radixSort(self, arr, mo):
            numeros = arr.copy()
    
            tInicio = time.time_ns()
            max1 = max(arr)
            exp = 1
            while max1/exp > 0:
                self.Radix.countingSort(mo,numeros,exp)
                exp *= 10
            tFin = time.time_ns()
            tiempo = tFin-tInicio    
            print(f"Radix:    {tiempo}    {self.recorridos}    {self.comparaciones}    {self.intercambios}")

    
class LamadaMetodos:
    
    def llamar(self, n):
        
        mo = MetodosOrdenamiento()
        mo.Burbuja.ordenacionBurbuja1(mo,n)
        mo.Burbuja.ordenacionBurbuja2(mo,n)
        mo.Burbuja.ordenacionBurbuja2(mo,n)
        mo.Insercion.ordenarInsercion(mo,n)
        mo.Seleccion.ordenamientoSeleccion(mo,n)
        mo.Quicksort.llamadaQuicksort(mo,n)
        mo.Shellsort.ordenar(mo,n)
        #mo.Radix.radixSort(mo,n,mo)
    
    
lm = LamadaMetodos()
na1 = NumerosAleatorios.generarNumeros(NumerosAleatorios, 1000)
na2 = NumerosAleatorios.generarNumeros(NumerosAleatorios, 10000)
na3 = NumerosAleatorios.generarNumeros(NumerosAleatorios, 100000)

print("----------------------1000 numeros----------------------")
print("Metodo        Tiempo      Rec     Comp      Int")
lm.llamar(na1)
print()
print("----------------------10000 numeros---------------------")
print("Metodo        Tiempo        Rec      Comp        Int")
lm.llamar(na2)
print()
print("----------------------100000 numeros--------------------")
print("Metodo        Tiempo          Rec      Comp        Int")
lm.llamar(na3)
    
